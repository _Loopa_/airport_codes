from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db_Airport_template import Airport, Base
import csv


engine = create_engine('mysql://root:pass@127.0.0.1:3316/', echo=True)
engine.execute('create database if not exists mydb')
engine = create_engine('mysql://root:pass@127.0.0.1:3316/mydb?charset=utf8mb4', echo=True)


Base.metadata.create_all(engine)
metadata = Base.metadata
airports_table = Airport.__table__


engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")


Session = sessionmaker(bind=engine)
s = Session()

'''newAirport1 = Airport("00A", "heliport", "Tot Heliport", "11", "NA", "US", "US-PA", "Bensal", "00A", None, "00A", "1,1")
newAirport2 = Airport("00B", "airport", "New airport", "20", "SA", "US", "US-PA", "Kernal", "00B", None, "00B", "1.25, "
                                                                                                                "20.3")
newAirport3 = Airport("00C", "Airfield", "Moscow", "1", "MO", "RU", "RU-MO", "Moscow", "00C", None, "00C", "60.3, 50.8")
s.add_all([newAirport1, newAirport2, newAirport3])
s.commit()
df = pd.read_csv(r'/home/loopa/Downloads/airport-codes_csv.csv', encoding='utf-8', parse_dates=['name'])
df.to_sql(con=engine, index_label='id', name=Airport.__tablename__, if_exists='replace')'''
with open('airport-codes_csv.csv', mode='r') as csv_file:
    reader = csv.DictReader(csv_file)
    for row in reader:
        latitude = row['coordinates'].split(', ')[0]
        longitude = row['coordinates'].split(', ')[1]
        s.add(Airport(row['ident'], row['type'], row['name'], row['elevation_ft'], row['continent'], row['iso_country'],
                      row['iso_region'], row['municipality'],  row['gps_code'], row['iata_code'], row['local_code'],
                      latitude, longitude))
s.commit()




