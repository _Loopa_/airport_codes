#!/bin/bash
sudo apt-get install libmysqlclient-dev python3-dev docker.io docker-compose mysql-client libmysqlclient-dev
sudo systemctl enable docker
pip install -r requirements.txt

sudo docker run --name=mysql -e MYSQL_ROOT_HOST='%' -e MYSQL_ROOT_USERNAME=root -e MYSQL_ROOT_PASSWORD=pass -v "$(pwd)/data:/var/lib/mysql" -p 3316:3306 --rm -d mysql:5.7 &

while ! mysqladmin ping -h"127.0.0.1" -P 3316 --silent; do
  echo 'ping'
  sleep 1
done

echo 'ready'

python ./db_init.py
python ./search.py UUBW
python ./distance.py UUEE UUDD
sudo docker stop mysql
